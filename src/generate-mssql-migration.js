const fs = require('fs');
const config = require('config');
const asyncConfig = require('config/async.js');
const db = require('@advanced-agrilytics/database');

async function go() {
  await asyncConfig.resolveAsyncConfigs(config)
    .catch((err) => {
      console.error('Failed to start application', err);
      throw err;
    });

    const allContents = fs.readFileSync('./data/name-tab.txt', 'utf-8');
    const model = await db.getModels().growerOperations;

    allContents.split(/\r?\n/).forEach(async (line, lineNumber) => {
        if (lineNumber !== 0) {
          const columns = line.split('\t');

          const tfName = columns[1];
          const newName = columns[4];

          if (tfName !== newName) {
                
            const operations = await model.findAll({
              where: {
                operationName: tfName
              }
            });

            if(operations.length != 1) {
              printOperationsWithDuplicates(tfName)
            } else {
              printMigrationString(operations[0].guid, newName)
            }
          }
        }
    });

    function printOperationsWithDuplicates(tfName) {
      fs.writeFile('duplicates.js', `{tfName}`, { flag: 'a' }, err => {});
    }

  function printMigrationString(guid, newName) {
    const content = `\nawait sequelize.query(
      'update GrowerOperations set OperationName = ? where Guid = ?',
      { replacements: ['${newName}', '${guid}'] }
    );` 
    fs.writeFile('output.js', content, { flag: 'a' }, err => {});
  }
  
}

go();
