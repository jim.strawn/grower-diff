const db = require('@advanced-agrilytics/database');
const config = require('config');
const asyncConfig = require('config/async.js');
const csv = require('csv-stringify/sync');
const fs = require('fs');
const mysql = require('./mysql');

async function go() {
  await asyncConfig.resolveAsyncConfigs(config)
  .catch((err) => {
    console.error('Failed to start application', err);
    throw err;
  });

  const headers = [
    'farmgate uuid', 
    'terraframing guid', 
    'farmgate operation',
    'terraframing operation',
    'precision agronomist', 
    'status',
  ];
  
  const sql = 'SELECT uuid, aa_guid, name, agronomist_id FROM operation;'
  const farmgateOperations = await mysql.query(sql, '');
  const results = farmgateOperations.map( farmgateOperation => 
    getFarmgateResults(farmgateOperation)
  );
  
  const resolved = await Promise.all(results).then(results);
  const filteredFarmgateResults = resolved.filter((result) => {
    return result.length !== 0
  });

  filteredFarmgateResults.unshift(headers);

  try {
    fs.writeFileSync('operation-name-mismatch-diff.csv', csv.stringify(filteredFarmgateResults));
  } catch (err) {
    console.error(err);
  };
  
  console.log('finished.....');
}

async function getFarmgateResults(farmgateOperation) {
  const model = await db.getModels().growerOperations;
  let terraframingOperation;
  if(farmgateOperation.aa_guid) {
    terraframingOperation = await model.findOne({
      where: {
        Guid: farmgateOperation.aa_guid,
      }
    });
  }

  if(!terraframingOperation) {
    return [];
  }
  else if(farmgateOperation.name != terraframingOperation.operationName) {
    const agronomistId = farmgateOperation.agronomist_id;
    const sql = 'SELECT first_name, last_name FROM user where id = ?;'
    const agronomist = await mysql.query(sql, agronomistId);
    const first = agronomist[0]?.first_name; 
    const last = agronomist[0]?.last_name; 

    let paName;
    if(!first && !last) {
      paName = `User not found. Id: ${agronomistId}`;   
    }
    else {
      paName = `${first} ${last}`;
    }
    return [ 
      farmgateOperation.uuid, 
      farmgateOperation.aa_guid, 
      farmgateOperation.name, 
      terraframingOperation.operationName,
      paName,
      'Name mismatch',
    ]
  }
  else {
    return [];
  }  
};

go();