const db = require('@advanced-agrilytics/database');
const config = require('config');
const asyncConfig = require('config/async.js');
const csv = require('csv-stringify/sync');
const fs = require('fs');
const mysql = require('./mysql')


async function go() {
  await asyncConfig.resolveAsyncConfigs(config)
  .catch((err) => {
    console.error('Failed to start application', err);
    throw err;
  });

  const headers = [
    'TFS guid', 
    'TFS operation',
    'Valid'
  ];
  
  const model = await db.getModels().growerOperations;
  const terraframingOperations = await model.findAll();
  const results = terraframingOperations.map( operation => 
    getResultsForOperation(operation)
  );
  
  const resolved = await Promise.all(results).then(results);
  const filteredResults = resolved.filter((result) => {
    return result.length !== 0
  });

  filteredResults.unshift(headers);

  try {
    fs.writeFileSync('tfs-operations-not-in-farmgate.csv', csv.stringify(filteredResults));
  } catch (err) {
    console.error(err);
  };
  
  console.log('finished.....');
}

async function getResultsForOperation(growerOperation) {
  const sql = 'SELECT uuid, name, agronomist_id FROM operation where aa_guid = ?'
  const result = await mysql.query(sql, growerOperation.guid);
  const farmgateOperation = result[0];
  if(!farmgateOperation) {
    const isActive = growerOperation.isActive === 1 ? 'true' : 'false';
    console.log('farmgateOperation:', farmgateOperation);
    return [ growerOperation.guid, growerOperation.operationName, isActive];
  }
  else {
    return [];
  }  
};

go();