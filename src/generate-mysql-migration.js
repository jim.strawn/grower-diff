const fs = require('fs');
const config = require('config');
const asyncConfig = require('config/async.js');
const mysql = require('./mysql');

async function go() {
  await asyncConfig.resolveAsyncConfigs(config)
    .catch((err) => {
      console.error('Failed to start application', err);
      throw err;
    });

  const allContents = fs.readFileSync('./data/name-tab.txt', 'utf-8');

  // const farmgateOperations = await mysql.query(sql, '');

  allContents.split(/\r?\n/).forEach(async (line, lineNumber) => {
    if (lineNumber !== 0) {
      const columns = line.split('\t');

      const fgName = columns[0].replaceAll('"', '');
      const newName = columns[4].replaceAll('"', '');

      if (fgName !== newName) {
        console.log(`fgName: ${fgName}, newName: ${newName}`);

        const sql = `SELECT uuid, name FROM operation WHERE name = '${fgName}';`
        const operations = await mysql.query(sql, '');

        printMigrationString(operations[0].uuid, newName)
      }
    }
  });

  function printMigrationString(guid, newName) {
    const content = `\nawait sequelize.query(
      'update operation set name = ? where uuid = ?',
      { replacements: ['${newName}', '${guid}'] }
    );` 
    fs.writeFile('output.js', content, { flag: 'a' }, err => {});
  }

}

go();
