const { asyncConfig } = require('config/async');
const {
  getSecret,
} = require('@advanced-agrilytics/aws-services/build/src/secrets-service');

module.exports = {
  mssql: {
    dialect: 'mssql',
    host: `mssql.advancedagtest.com`,
    username: 'gisadmin',
    password: asyncConfig(getSecret.bind(null, 'test/db/theforce/mssql', 'gisadmin')),
    database: 'theforce',
    schema: 'dbo',
    dialectOptions: {
      options: {
        requestTimeout: 300000, // 5 minutes
      },
    },
  },
  mysql: {
    host: 'farmgate-mysql.advancedagtest.com',
    dialect: 'mysql',
    database: 'crm',
    username: 'crm-application',
    password: asyncConfig(getSecret.bind(null, 'test/db/farmgate/mysql', 'crm-application')),
  },
  postgres: {},
};
